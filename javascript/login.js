
function submitLogin() {
var usrn = document.getElementById("username").value; 
var passw = document.getElementById("password").value; 

        var url = "login_process.php";
        var params = 'usrn=' + usrn + "&passw=" + passw;
        
        var target = document.getElementById("loginShow");
        var xhr = new XMLHttpRequest();
        
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
            
            if(xhr.responseText == "valid"){
              document.getElementById("stockSection").style.display = 'block';
              
              target.innerHTML = "You're Logged in as: " + usrn;
              document.getElementById("loginSection").style.display = 'none';
            }

            else{
              target.innerHTML = "The Username or Password you entered is not valid. Try Again.";
            }

          }
        }
        
        xhr.send(params);

        return target;
      }

