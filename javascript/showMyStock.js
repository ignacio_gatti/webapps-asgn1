function replaceABC() {
                
        var companynametext = document.getElementById("ABCaction").value; 
        
        var url = "stockProcess.php";
        var params = '?companynametext=' + companynametext;
        var target = document.getElementById("Row2");
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + params, true);
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            target.innerHTML = xhr.responseText;
            
          document.getElementById("ABC_Comment").style.display = 'block';

          document.getElementById("XYZ_Comment").style.display = 'none';
          document.getElementById("Acme_Comment").style.display = 'none';
          document.getElementById("Fling_Comment").style.display = 'none';
          document.getElementById("Neutral_Comment").style.display = 'none';
          document.getElementById("TSI_Comment").style.display = 'none';  

          }
        }
        
        xhr.send();
        
        return target;
      }

function replaceXYZ() {
        var companynametext = "XYZ Logistics";
        var url = "stockProcess.php";
        var params = '?companynametext=' + companynametext;
        var target = document.getElementById("Row2");
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + params, true);
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            target.innerHTML = xhr.responseText;
            
            document.getElementById("XYZ_Comment").style.display = 'block';
          
          document.getElementById("ABC_Comment").style.display = 'none';
          document.getElementById("Acme_Comment").style.display = 'none';
          document.getElementById("Fling_Comment").style.display = 'none';
          document.getElementById("Neutral_Comment").style.display = 'none';
          document.getElementById("TSI_Comment").style.display = 'none';
          }
        }
        
        xhr.send();

        return target;
        
       
      }

function replaceAcme() {

     var companynametext = "Acme Publishing";
        
        var url = "stockProcess.php";
        var params = '?companynametext=' + companynametext;
        var target = document.getElementById("Row2");
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + params, true);
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            target.innerHTML = xhr.responseText;

          document.getElementById("Acme_Comment").style.display = 'block';
          
          document.getElementById("ABC_Comment").style.display = 'none';
          document.getElementById("XYZ_Comment").style.display = 'none';
          document.getElementById("Fling_Comment").style.display = 'none';
          document.getElementById("Neutral_Comment").style.display = 'none';
          document.getElementById("TSI_Comment").style.display = 'none';

          }
        }
        
        xhr.send();

        return target;
     
      }

function replaceFling() {
       var companynametext = "Fling Fing";
        var url = "stockProcess.php";
        var params = '?companynametext=' + companynametext;
        var target = document.getElementById("Row2");
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + params, true);
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            target.innerHTML = xhr.responseText;

          document.getElementById("Fling_Comment").style.display = 'block';
          
          document.getElementById("ABC_Comment").style.display = 'none';
          document.getElementById("XYZ_Comment").style.display = 'none';
          document.getElementById("Acme_Comment").style.display = 'none';
          document.getElementById("Neutral_Comment").style.display = 'none';
          document.getElementById("TSI_Comment").style.display = 'none';
          }
        }
        
        xhr.send();

        return target;
       
        
      }
      
function replaceNeutral() {
        var companynametext = "Neutral Networks";
        var url = "stockProcess.php";
        var params = '?companynametext=' + companynametext;
        var target = document.getElementById("Row2");
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + params, true);
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            target.innerHTML = xhr.responseText;

          document.getElementById("Neutral_Comment").style.display = 'block';
          
          document.getElementById("ABC_Comment").style.display = 'none';
          document.getElementById("XYZ_Comment").style.display = 'none';
          document.getElementById("Acme_Comment").style.display = 'none';
          document.getElementById("Fling_Comment").style.display = 'none';
          document.getElementById("TSI_Comment").style.display = 'none';
          }
        }
        
        xhr.send();

        return target;
        
        
      }   

function replaceTSI() {
        var companynametext = "Total Solutions Inc";
        var url = "stockProcess.php";
        var params = '?companynametext=' + companynametext;
        var target = document.getElementById("Row2");
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + params, true);
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            target.innerHTML = xhr.responseText;

          document.getElementById("TSI_Comment").style.display = 'block';
          
          document.getElementById("ABC_Comment").style.display = 'none';
          document.getElementById("XYZ_Comment").style.display = 'none';
          document.getElementById("Acme_Comment").style.display = 'none';
          document.getElementById("Fling_Comment").style.display = 'none';
          document.getElementById("Neutral_Comment").style.display = 'none';
          }
        }
        
        xhr.send();

        return target;
        
        
      }   